#include <iostream>
#include <string>
#include <glm/gtx/string_cast.hpp>
// GLEW
#include <GL/glew.h>
// GLFW
#include <GLFW/glfw3.h>

// GLM Mathematics
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <stdio.h>      /* printf, scanf, puts, NULL */
#include <stdlib.h>     /* srand, rand */
#include <time.h>

// Shader loading class
#include "Shader.h"
// Camera loading class
#include "Camera.h"
// Model loading class
#include "Model.h"

// Other Libs
#include <SOIL.h>

// Dimension of the window
const GLuint WIDTH = 1000, HEIGHT = 800;
// initialize nb islets
GLint nbzones(4);
// initialize car speed
GLfloat car_speed(6.0f);
// initialize crepuscule angle
GLfloat ANGLE_CREPUSC(3*M_PI/5);
// initialize aube angle
GLfloat ANGLE_AUBE(4*M_PI/3);
// time cucle
GLint DUREE_CYCLE(24);
// type of islet
enum Type_zone{
    Type1,
    Type2,
    Type3,
    Type4,
    foret,
};

int main()
{
    srand (static_cast  <unsigned> (time(0)));
    std::cout << "Starting GLFW context, OpenGL 3.3" << std::endl;
    glfwInit();

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3); //3
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3); //.3
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    #ifdef __APPLE__
      glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    #endif
    glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
    
    GLFWwindow* window = glfwCreateWindow(WIDTH, HEIGHT, "MyWindow", nullptr, nullptr);    
    if (window == nullptr)
    {
        std::cout << "Failed to create GLFW window" << std::endl;
        glfwTerminate();
        return -1;
    }
    
    glfwMakeContextCurrent(window);
    glfwSetKeyCallback(window, key_callback);

    glewExperimental = GL_TRUE;
    if (glewInit() != GLEW_OK)
    {
        std::cout << "Failed to initialize GLEW" << std::endl;
        return -1;
    }    

    int width, height;
    glfwGetFramebufferSize(window, &width, &height); 
    glViewport(0, 0, width, height);

    glEnable(GL_DEPTH_TEST);
    
    Shader shaders("shaders/c3/default.vertexshader", 
        "shaders/c3/default.fragmentshader");
    

   GLfloat vertices[] = {
        /*     Positions    |      Normales     |     UV     */
        -500.0f,  0.0f, -500.0f,   0.0f, 1.0f, 0.0f,   0.0f, 1250.0f, // Top Left
        -500.0f,  0.0f,  500.0f,   0.0f, 1.0f, 0.0f,   0.0f, 0.0f, // Bottom Left
         500.0f,  0.0f, -500.0f,   0.0f, 1.0f, 0.0f,   1250.0f, 1250.0f, // Top Right
         500.0f,  0.0f,  500.0f,   0.0f, 1.0f, 0.0f,   1250.0f, 0.0f,  // Bottom Right
    };

    GLshort indices[]{
        0, 1, 2,
        1, 3, 2,
    };
 
    
    // Declare the identifiers of our VAO, VBO and EBO
    GLuint VAO, VBO, EBO;
    // Inform OpenGL to generate one VAO
    glGenVertexArrays(1, &VAO);
    // Inform OpenGL to generate one VBO
    glGenBuffers(1, &VBO);
    // Inform OpenGL to generate one EBO
    glGenBuffers(1, &EBO); 
    
    // Bind the VAO
    glBindVertexArray(VAO);
    // Bind and fill the VBO
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
    // Bind and fill the EBO
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

    // Position attribute
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), (GLvoid*)0);
    glEnableVertexAttribArray(0);
    // Color attribute
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), 
        (GLvoid*)(3 * sizeof(GLfloat)));
    glEnableVertexAttribArray(1);
    // UV coordinates
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), 
        (GLvoid*)(6 * sizeof(GLfloat)));
    glEnableVertexAttribArray(2);
    
    // VBO is detached from the current buffer in the OpenGL context
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    // VAO is detached from the current object in the OpenGL context
    glBindVertexArray(0);
    // EBO is detached (the last one!)
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0); 

    // Declare the texture identifier
    GLuint texture; 
    // Generate the texture
    glGenTextures(1, &texture); 
    // Bind the texture created in the global context of OpenGL
    glBindTexture(GL_TEXTURE_2D, texture); 
    // Method of wrapping
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT); 
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT); 
    // Filtering method
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR); 
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    
    // Loading the image file using the SOIL lib
    int twidth, theight;
    unsigned char* data = SOIL_load_image("texture/texture4.jpg",
        &twidth, &theight, 0, SOIL_LOAD_RGB);
    // Associate the image data with texture
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, twidth, theight, 
        0, GL_RGB, GL_UNSIGNED_BYTE, data);
    // Generate the mipmap
    glGenerateMipmap(GL_TEXTURE_2D);
    // Free the memory
    SOIL_free_image_data(data);
    // Unbind the texture
    glBindTexture(GL_TEXTURE_2D, 0);

    // Camera
    Camera camera(glm::vec3(0.0f, 0.5f, 3.0f), window);
    // Model importation
    char* path = "model/house/farmhouse_obj.obj";
    Model maison("model/House/farmhouse_obj.obj");
    Model soleil("model/Sun/soleil.obj");
    Model lamp("model/Lamp/Lamp.obj");
    Model lit_lamp("model/Lamp/litLamp/Lamp.obj");
    Model arbre1("model/Trees/Tree1/Tree1.3ds");
    Model car("model/Models_E0502A097/kamaz.3ds");

    


    GLfloat xzone[nbzones];
    GLfloat zzone[nbzones];
    Type_zone typezone[nbzones];
    GLint intervalle(34);
    for(int i(0); i < nbzones; i++){

        GLint j(0); 
        GLint n(i);
        GLint k(4);

        while(k<=n){ 
            j++;     
            n-=k;
            k+=8;
        }


        GLint l = 2 * (j+1); 
        GLint xzMax = intervalle*(j + 0.5f); 

        if(n < l-1){ 
            xzone[i] = -xzMax;
            zzone[i] =  xzMax - intervalle*(n + 1);
        } else if( l-1 <= n &&  n < 2*l - 2 ){ 
            zzone[i] = -xzMax;
            xzone[i] = -xzMax + intervalle*(n - (l-2));
        } else if( 2*l-2 <= n && n < 3*l - 3){ 
            xzone[i] =  xzMax;
            zzone[i] = -xzMax + intervalle*(n - (2*l-3));
        } else{ 
            zzone[i] = xzMax;
            xzone[i] = xzMax - intervalle*(n - (3*l-4));
        }

        if(j==0){      
            if(n==0){  
                xzone[i] = -xzMax;
                zzone[i] = -xzMax;
            } else if(n==1){
                xzone[i] =  xzMax;
                zzone[i] = -xzMax;
            } else if(n==2){
                xzone[i] =  xzMax;
                zzone[i] =  xzMax;
            } else if(n==3){
                xzone[i] = -xzMax;
                zzone[i] =  xzMax;
            }
        }


        GLint prob = rand()%101;

        if (prob < 25){
            typezone[i] = Type1;
        } else if (25 <= prob && prob < 25 + 20){
            typezone[i] = Type2;
        } else if (25 + 20 <= prob
                   && prob < 25 + 30 + 20){
            typezone[i] = Type3;
        }else{
            typezone[i] = foret;
        }
    }


    glm::vec3 carPos = glm::vec3(xzone[0] + 17.0f, 0.1f, zzone[0] + 13.0f);

    glm::vec3 carFront = glm::vec3(0.0f, 0.0f, -1.0f);
    

    
    GLfloat deltaT(0);
    GLfloat deltaT2(0);
    GLfloat lastT;
    lastT = glfwGetTime();



    while (!glfwWindowShouldClose(window))
    {

        glfwPollEvents();
        camera.Do_Movement();
        camera.Switch_Mode();

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        shaders.Use();
        GLint modelLoc = glGetUniformLocation(shaders.Program, "model");
        GLint viewLoc  = glGetUniformLocation(shaders.Program, "view");
        GLint projLoc  = glGetUniformLocation(shaders.Program, "projection");
        glm::mat4 model = glm::mat4(1.0f);
     
        
        glm::mat4 projection = glm::perspective(45.0f, (float)WIDTH/(float)HEIGHT, 0.1f, 350.0f);
        glm::mat4 view = camera.GetViewMatrix();
        model = glm::translate(model, glm::vec3(0.0f, 0.0f, 0.0f));	
        glUniformMatrix4fv(glGetUniformLocation(shaders.Program, "model"), 1, GL_FALSE, glm::value_ptr(model));

        glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));
        glUniformMatrix4fv(projLoc, 1, GL_FALSE, glm::value_ptr(projection));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));



        glBindVertexArray(VAO);
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, texture);
        glUniform1i(glGetUniformLocation(shaders.Program, "modelTexture"), 0);
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_SHORT, 0);
        glBindVertexArray(0);
        GLint lightPos = glGetUniformLocation(shaders.Program, "lightPos");
        GLint lightColor = glGetUniformLocation(shaders.Program, "lightColor");
        GLint ambientStrength = glGetUniformLocation(shaders.Program, "ambientStrength");
        GLint specularStrength = glGetUniformLocation(shaders.Program, "specularStrength");
        GLfloat ambStr(0.05f);
        GLfloat specStr(0.3f);
        glm::vec4 lPos(camera.Position.x, camera.Position.y + 300.0f, camera.Position.z, 1.0f);
        glm::vec3 lColor(1.0f, 1.0f, 1.0f);
        glm::mat4 rot;
        GLfloat angle = glm::mod(2*M_PI*(glfwGetTime()/DUREE_CYCLE), 2*M_PI);
        rot = glm::rotate(rot, angle, glm::vec3(0.0f, 0.0f, 1.0f));
        lPos = rot * lPos;

        glm::vec3 clearColor;
        // manage the light throught day and night
        if (angle > ANGLE_CREPUSC && angle < ANGLE_AUBE){
            lColor = glm::vec3(0.0f, 0.0f, 0.05f);
            clearColor = glm::vec3(0.0f, 0.0f, 0.1f);
            ambStr = 0.05f;
        } else if (angle > M_PI/4 && angle < ANGLE_CREPUSC){
            double x = (ANGLE_CREPUSC - angle)/(ANGLE_CREPUSC - M_PI/4);
            lColor = glm::vec3(x * 1.0f, pow(x, 2) * 1.0f, pow(x, 4) * 1.0f + 0.05f);

            clearColor = glm::vec3((-2*pow(x,2) + 2*x) * 1.0f, pow(x, 2) * 0.5f, pow(x, 4) * 1.0f + 0.1f);

            ambStr = x * 0.65f + 0.05f;
        } else if (angle > ANGLE_AUBE && angle < 5*M_PI/3){
            double x = (ANGLE_AUBE - angle)/(ANGLE_AUBE - 5*M_PI/3);
            lColor = glm::vec3(pow(x,4) * 1.0f, pow(x, 2) * 1.0f, x * 1.0f + 0.05f);
            clearColor = glm::vec3((-2*pow(x,2) + 2*x) * 1.0f, pow(x, 2) * 0.5f, pow(x, 4) * 1.0f + 0.1f);
            ambStr = x * 0.65f + 0.05f;


        } else{
            lColor = glm::vec3(1.0f, 1.0f, 1.0f);
            clearColor = glm::vec3(0.0f, 0.5f, 1.0f);
            ambStr = 0.7f;
        }
        glClearColor(clearColor.x, clearColor.y, clearColor.z, 1.0f);
        glUniform3f(lightPos, lPos.x, lPos.y, lPos.z);
        glUniform3f(lightColor, lColor.x, lColor.y , lColor.z);
        glUniform1f(ambientStrength, ambStr);
        glUniform1f(specularStrength, specStr);
        
        glUniformMatrix4fv(glGetUniformLocation(shaders.Program, "projection"), 1, GL_FALSE, glm::value_ptr(projection));
        glUniformMatrix4fv(glGetUniformLocation(shaders.Program, "view"), 1, GL_FALSE, glm::value_ptr(view));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));

        GLint lampPos = glGetUniformLocation(shaders.Program, "lampPos");
        GLint lampColor = glGetUniformLocation(shaders.Program, "lampColor");
        glm::vec3 lmpColor;
        if (angle > 2*M_PI/5 && angle < 8*M_PI/5){
            lmpColor = glm::vec3(1.0f, 0.56f, 0.17f);
        } else {
            lmpColor = glm::vec3(0.0f, 0.0f, 0.0f);
        }
        glUniform3f(lampColor, lmpColor.x, lmpColor.y , lmpColor.z);

        glm::vec3 lmpPos(0.0f, 1.6f, -4.0f);
        glUniform3f(lampPos, lmpPos.x, lmpPos.y , lmpPos.z);

        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(lmpPos.x, 0.0f, lmpPos.z));
        model = glm::scale(model, glm::vec3(1.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        if(lmpColor.x != 0.0f){
            glUniform1f(ambientStrength, 0.7f);
            glUniform3f(lightColor, lmpColor.x, lmpColor.y, lmpColor.z);

            lit_lamp.Draw(shaders);
            glUniform1f(ambientStrength, ambStr);
            glUniform3f(lightColor, lColor.x, lColor.y , lColor.z);

        } else{
            lamp.Draw(shaders);
        }

        // manage the car position
        deltaT = glfwGetTime() - lastT;
        lastT = glfwGetTime();
        model = glm::mat4(1.0f);
        model = glm::rotate(model,-(carFront.z - glm::length(carFront.z)+ carFront.x) * (GLfloat)M_PI/2,
                            glm::vec3(0.0f, 1.0f, 0.0f));
        GLint dir;
        dir = 0;
        if(glm::length(carPos.x) + 34.0f >= (sqrt(nbzones))*34.0f + deltaT*car_speed
                || carPos.z + 30.0f >= (sqrt(nbzones))*34.0f + deltaT*car_speed
                || carPos.z - 38.0f <= -(sqrt(nbzones))*34.0f + deltaT*car_speed){

            dir = 0 ;
        }

        if(carFront.x != 0 && glm::mod(carPos.x, 34.0f) <= deltaT*car_speed){
            if(dir == 0){
                carFront = glm::vec3(0.0f, 0.0f, carFront.x);
                carPos.x += carFront.x * deltaT * car_speed;
                carPos.z -= carFront.z * deltaT * car_speed;
            }
        } else if(carFront.z != 0 && glm::mod(carPos.z + 4.0f, 34.0f) <= deltaT*car_speed){
            if(dir == 0){
                carFront = glm::vec3(-carFront.z, 0.0f, 0.0f);
                carPos.x += carFront.x * deltaT * car_speed;
                carPos.z -= carFront.z * deltaT * car_speed;
            }
        } else {
            carPos.x += carFront.x * deltaT * car_speed;
            carPos.z += carFront.z * deltaT * car_speed;
        }
        glm::mat4 mouvVoiture;
        mouvVoiture = glm::translate(mouvVoiture,
                                        glm::vec3(carPos.x - carFront.z * 0.9f,
                                                carPos.y,
                                                carPos.z + carFront.x * 0.9f));
        model = mouvVoiture*model;
        model = glm::scale(model, glm::vec3(0.3f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        car.Draw(shaders);

        deltaT = glfwGetTime() - lastT;
        lastT = glfwGetTime();
        model = glm::mat4(1.0f);
        model = glm::rotate(model,carFront.z - glm::length(carFront.z)+ carFront.x * (GLfloat)M_PI/2,
                            glm::vec3(0.0f, 1.0f, 0.0f));
       
        // manage each type of islet (for each type )
        GLint rotMais2(0);
        for(int i(0); i < nbzones; i++){

            if(typezone[i]==Type1){
                GLfloat x, y, z;
                for (int house(0); house < 8; house++){
                    if(house<3){
                        model = glm::mat4(1.0f);
                        x = xzone[i] - 5.0f + house*10.0f;
                        y = 0.0f;
                        z = zzone[i] - 10.0f;
                        model = glm::translate(model, glm::vec3(x, y, z));
                        model = glm::rotate(model, (GLfloat) M_PI, glm::vec3(0.0f, 1.0f, 0.0f));
                        model = glm::scale(model, glm::vec3(0.1f));

                    } else if(3<=house && house<6){
                        model = glm::mat4(1.0f);
                        x = xzone[i] - 5.0f + (house-3)*10.0f;
                        y = 0.0f;
                        z = zzone[i] + 10.0f;
                        model = glm::translate(model, glm::vec3(x, y, z));
                        model = glm::scale(model, glm::vec3(0.1f));

                    } else if(6 <= house && house<8){
                        model = glm::mat4(1.0f);
                        x = xzone[i] + 10.2f;
                        y = 0.0f;
                        z = zzone[i] + 1.25f - (house-6)*10.0f;
                        model = glm::translate(model, glm::vec3(x, y, z));
                        model = glm::rotate(model, (GLfloat) M_PI/2, glm::vec3(0.0f, 1.0f, 0.0f));
                        model = glm::scale(model, glm::vec3(0.1f));

                    } else {
                        model = glm::mat4(1.0f);
                        x = xzone[i] - 4.0f;
                        y = 0.0f;
                        z = zzone[i] + 1.25f - (house-8)*8.0f;
                        model = glm::translate(model, glm::vec3(x, y, z));
                        model = glm::rotate(model, (GLfloat) -M_PI/2, glm::vec3(0.0f, 1.0f, 0.0f));
                        model = glm::scale(model, glm::vec3(0.1f));
                    }
                    glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
                    maison.Draw(shaders);

                    for (int tree(0); tree < 3; tree++){
                        model = glm::mat4(1.0f);
                        model = glm::translate(model,
                                               glm::vec3(xzone[i] + pow(-1,tree)*(i%4 +1)*1.5f + (tree*i)%3*1.4,
                                                         0.0f,
                                                         zzone[i] + pow(-1,tree)*(i%3 + 1)*tree - 3.0f + pow(-1,i)*(tree%3)*0.8));
                        model = glm::rotate(model, (GLfloat) M_PI/2, glm::vec3(-1.0f, 0.0f, 0.0f));
                        model = glm::scale(model, glm::vec3(1.3f));
                        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
                        arbre1.Draw(shaders);
                    }
                }
            }else if(typezone[i]==Type2){

                    model = glm::mat4(1.0f);
                    model = glm::translate(model, glm::vec3(xzone[i], 0.0f, zzone[i] - 6.0f));
                    model = glm::rotate(model, (GLfloat) (rotMais2*M_PI/2), glm::vec3( 0.0f, 1.0f, 0.0f));
                    model = glm::scale(model, glm::vec3(0.1f));
                    glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
                    maison.Draw(shaders);

                    model = glm::mat4(1.0f);
                    model = glm::translate(model, glm::vec3(xzone[i]-6.0f, 0.0f, zzone[i]+3.0f));
                    model = glm::rotate(model, (GLfloat) (M_PI/2), glm::vec3(0.0f, 1.0f, 0.0f));
                    model = glm::scale(model, glm::vec3(0.1f));
                    glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
                    maison.Draw(shaders);

                    model = glm::mat4(1.0f);
                    model = glm::translate(model, glm::vec3(xzone[i]+2.0f, 0.0f, zzone[i]+1.0f));
                    model = glm::rotate(model, (GLfloat) M_PI/2, glm::vec3(-1.0f, 0.0f, 0.0f));
                    model = glm::scale(model, glm::vec3(1.3f));
                    glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
                    arbre1.Draw(shaders);
                    model = glm::mat4(1.0f);
                    model = glm::translate(model, glm::vec3(xzone[i]+2.0f, 0.0f, zzone[i]+1.0f));
                    model = glm::rotate(model, (GLfloat) M_PI/2, glm::vec3(-1.0f, 0.0f, 0.0f));
                    model = glm::scale(model, glm::vec3(1.3f));
                    glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
                    arbre1.Draw(shaders);

                    
                    rotMais2++;
            }else if(typezone[i] == foret){
                GLfloat x, y, z;
                for (int tree(0); tree < 16; tree++){
                    model = glm::mat4(1.0f);
                    x = xzone[i] - 13.0f + (tree%5)*6.5;
                    y = 0.0f;
                    z = zzone[i] - 6.5f + (tree%3)*6.5 - 4.0f;
                    model = glm::translate(model, glm::vec3(x, y, z));
                    model = glm::rotate(model, (GLfloat) M_PI/2, glm::vec3(-1.0f, 0.0f, 0.0f));
                    model = glm::scale(model, glm::vec3(1.3f));
                    
                    glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
                    arbre1.Draw(shaders);
                }
            }else if(typezone[i]==Type3){
                GLfloat x, y, z;
                for (int tree(0); tree < 16; tree++){
                    if(tree < 10){
                        model = glm::mat4(1.0f);
                        x = xzone[i] - 13.0f + (tree%5)*6.5;
                        y = 0.0f;
                        z = zzone[i] + pow(-1,floor(tree/5))*13.0f - 4.0f;
                        model = glm::translate(model, glm::vec3(x, y, z));
                        model = glm::rotate(model, (GLfloat) M_PI/2, glm::vec3(-1.0f, 0.0f, 0.0f));
                        model = glm::scale(model, glm::vec3(1.3f));
                    } else{
                        model = glm::mat4(1.0f);
                        x = xzone[i] + pow(-1,floor(tree/3))*13.0;
                        y = 0.0f;
                        z = zzone[i] - 6.5f + (tree%3)*6.5 - 4.0f;
                        model = glm::translate(model, glm::vec3(x, y, z));
                        model = glm::rotate(model, (GLfloat) M_PI/2, glm::vec3(-1.0f, 0.0f, 0.0f));
                        model = glm::scale(model, glm::vec3(1.3f));
                    }
                    glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
                    arbre1.Draw(shaders);
                }
                // House1
                model = glm::mat4(1.0f);
                model = glm::translate(model, glm::vec3(xzone[i], 0.0f, zzone[i] - 6.0f));
                model = glm::rotate(model, (GLfloat) (M_PI/2), glm::vec3( 0.0f, 1.0f, 0.0f));
                model = glm::scale(model, glm::vec3(0.1f));
                glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
                maison.Draw(shaders);
                // House2
                model = glm::mat4(1.0f);
                model = glm::translate(model, glm::vec3(xzone[i]-6.0f, 0.0f, zzone[i]+3.0f));
                model = glm::rotate(model, (GLfloat) -(M_PI/2), glm::vec3(0.0f, 1.0f, 0.0f));
                model = glm::scale(model, glm::vec3(0.1f));
                glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
                maison.Draw(shaders);

                // House3
                model = glm::mat4(1.0f);
                model = glm::translate(model, glm::vec3(xzone[i]+6.0f, 0.0f, zzone[i]-3.0f));
                model = glm::rotate(model, (GLfloat) (M_PI/2), glm::vec3(0.0f, 1.0f, 0.0f));
                model = glm::scale(model, glm::vec3(0.1f));
                glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
                maison.Draw(shaders);

                // House4
                model = glm::mat4(1.0f);
                model = glm::translate(model, glm::vec3(xzone[i]-6.0f, 0.0f, zzone[i]-8.0f));
                model = glm::rotate(model, (GLfloat) (M_PI/4), glm::vec3(0.0f, 1.0f, 0.0f));
                model = glm::scale(model, glm::vec3(0.1f));
                glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
                maison.Draw(shaders);

                
            }else if(typezone[i] == Type4){
                GLfloat x, y, z;
                for (int tree(0); tree < 16; tree++){
                    model = glm::mat4(1.0f);
                    x = xzone[i] - 13.0f + (tree%5)*6.5;
                    y = 0.1f;
                    z = zzone[i] - 6.5f + (tree%3)*6.5 - 4.0f;
                    model = glm::translate(model, glm::vec3(x, y, z));
                    model = glm::scale(model, glm::vec3(0.1f));

                    glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
                    maison.Draw(shaders);
                }
            }
            
        }
        glBindVertexArray(0);
        glfwSwapBuffers(window);


    }

    glDeleteVertexArrays(1, &VAO);
    glDeleteBuffers(1, &VBO);
    glDeleteBuffers(1, &EBO);
    
    glfwTerminate();
    return 0;
}
