Module Dynamic light : 

* A light source that moves in the "sky",
* A fixed light during the "night" (preferably coming from a street lamp!)

Module realistic camera : 

* Add a visualization mode where the camera is situated in the air and pointing
permanently to the ground (i.e. aerial imaging),
* allow changing the viewing mode by pressing a key on the keyboard.

Module Random generation of the world : 

* Make sure that the world is generated randomly at the start of the application:
the different objects are randomly placed in the space, but consistent (no
levitation or collision between objects).

Module Adding planned moves : 

* Move one vehicle according to a predefined or automatic pattern 


How to compile the programme 

- cmake .
- make 
- ./course3